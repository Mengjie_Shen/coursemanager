import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mengjie
 * Date : 2018/1/16
 * Time : 21:45
 **/
public class Course {

    public String courseID;
    public String courseName;
    public int semester;
    public List<Integer> lectures;
    public List<Integer> labs;
    public List<Integer> tutorials;

    public Course(String courseID, String courseName, int semester) {
        this.courseID = courseID;
        this.courseName =courseName;
        this.semester = semester;
        this.lectures = new ArrayList<>();
        this.labs = new ArrayList<>();
        this.tutorials = new ArrayList<>();

    }

    @Override
    public String toString() {
        String returnStr = "";
        returnStr += this.courseID + "\t";
        returnStr += this.courseName + "\t";
        returnStr += this.semester + "\t";
        returnStr += this.lectures + "\t";
        returnStr += this.labs + "\t";
        returnStr += this.tutorials;

        return returnStr;
    }
}
