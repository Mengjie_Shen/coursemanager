import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;

/**
 * Created by Mengjie
 * Date : 2018/1/16
 * Time : 21:48
 **/
public class CourseManager {

    private List<Course> technicalCourses = new ArrayList<>();
    private List<Course> complementaryCourses = new ArrayList<>();
    private Course course;
    private int indexNum = 1;

    public void start() {

        //COMPSCI 751
        course = new Course("COMPSCI 751", "Advanced Topics in Database Systems", 1);
        course.lectures.addAll(Arrays.asList(317,417,511));
        course.labs.addAll(Arrays.asList(116,117,415,416));
        technicalCourses.add(course);
        System.out.println(course.toString());

        //COMPSCI 760
        course = new Course("COMPSCI 760", "Datamining and Machine Learning", 1);
        course.lectures.addAll(Arrays.asList(109,208,416));
        technicalCourses.add(course);
        System.out.println(course.toString());

        //COMPSCI 752
        course = new Course("COMPSCI 752", "Web Data Management", 1);
        course.lectures.addAll(Arrays.asList(214,310));
        technicalCourses.add(course);
        System.out.println(course.toString());

        //COMPSCI 734
        course = new Course("COMPSCI 734", "Web, Mobile and Enterprise Computing", 1);
        course.lectures.addAll(Arrays.asList(111,315,516));
        course.tutorials.addAll(Arrays.asList(116,510));
        technicalCourses.add(course);
        System.out.println(course.toString());

        //////////////////////////////////////////////////////////////////////////////

        //INFOSYS 722
        course = new Course("INFOSYS 722", "Data Mining and Big Data", 2);
        course.lectures.addAll(Arrays.asList(214,215,216));
        course.labs.addAll(Arrays.asList(316,317,412,413,515,516));
        technicalCourses.add(course);
        System.out.println(course.toString());

        //COMPSCI 760
        course = new Course("COMPSCI 760", "Datamining and Machine Learning", 2);
        course.lectures.addAll(Arrays.asList(113,314,514));
        technicalCourses.add(course);
        System.out.println(course.toString());

        //COMPSCI 761
        course = new Course("COMPSCI 761", "Advanced Topics in Artificial Intelligence", 2);
        course.lectures.addAll(Arrays.asList(310,410,510));
        technicalCourses.add(course);
        System.out.println(course.toString());

        //COMPSCI 753
        course = new Course("COMPSCI 753", "Uncertainty in Data", 2);
        course.lectures.addAll(Arrays.asList(313,513));
        course.tutorials.addAll(Arrays.asList(312));
        technicalCourses.add(course);
        System.out.println(course.toString());

        //////////////////////////////////////////////////////////////////////////////

        //INFOSYS 700
        course = new Course("INFOSYS 700", "Digital Innovation", 1);
        course.lectures.addAll(Arrays.asList(410,411,412));
        complementaryCourses.add(course);
        System.out.println(course.toString());

        //HLTINFO 723
        course = new Course("HLTINFO 723", "Health Knowledge Management", 1);
        course.lectures.addAll(Arrays.asList(109,110));
        complementaryCourses.add(course);
        System.out.println(course.toString());

        //HLTINFO 728
        course = new Course("HLTINFO 728", "Principles of Health Informatics", 1);
        course.lectures.addAll(Arrays.asList(309,310));
        complementaryCourses.add(course);
        System.out.println(course.toString());

        //HLTINFO 730
        course = new Course("HLTINFO 730", "Healthcare Decision Support Systems", 2);
        course.lectures.addAll(Arrays.asList(109,110));
        complementaryCourses.add(course);
        System.out.println(course.toString());

        //////////////////////////////////////////////////////////////////////////////

        //INFOSYS 700
        course = new Course("INFOSYS 700", "Digital Innovation", 2);
        course.lectures.addAll(Arrays.asList(109,110,111));
        complementaryCourses.add(course);
        System.out.println(course.toString());

        //INFOSYS 701
        course = new Course("INFOSYS 701", "Global Outsourcing", 2);
        course.lectures.addAll(Arrays.asList(410,411,412));
        complementaryCourses.add(course);
        System.out.println(course.toString());


        List<Course> allCourses = technicalCourses;
        allCourses.addAll(complementaryCourses);

        System.out.println("*********************************************");
        for (Course course : allCourses) {
            System.out.println(course.toString());
        }

        List<List<Course>> c = combinations(allCourses, 8);
        List<Course> courses = new ArrayList<>();

        for (int i = 0; i < allCourses.size(); i++) {
            for (int j = 0; j < allCourses.size(); j++) {

            }

        }
        for (Course course : allCourses) {
            if (courses.size() == 8) {
                c.add(courses);
                courses = new ArrayList<>();
            }else {
                courses.add(course);
            }
        }

        System.out.println("line 133: size of List<List<Course>> c is: " + c.size());

        for (List<Course> cl : c) {
            boolean hasClash = hasClash(cl);
            boolean semesterBalance = semesterBalance(cl);
            if (!hasClash && semesterBalance) {
                for (int i = 0; i < cl.size(); i++) {
                    System.out.println(indexNum + "\t" + cl.get(i).toString());
                }
                indexNum++;
            }
        }


    }

    public boolean hasClash(List<Course> cl){

            for (int i = 0; i < cl.size(); i++) {
                for (int j = 0; j < cl.size(); j++) {
                    Course c1 = cl.get(i);
                    Course c2 = cl.get(j);
                    if (c1.semester == c2.semester && i != j) {
                        for (Integer lectureTime : c1.lectures) {
                            if (c2.lectures.contains(lectureTime) || c2.labs.contains(lectureTime) || c2.tutorials.contains(lectureTime)) {
//                                System.out.println(i + " vs " + j);
//                                System.out.println("CLASH!!\n" + c1.toString() + "\n" + c2.toString());
                                return true;
                            }
                        }

                        for (Integer labTime : c1.labs) {
                            if (c2.lectures.contains(labTime) || c2.labs.contains(labTime) || c2.tutorials.contains(labTime)) {
                                return true;
                            }
                        }

                        for (Integer tutorialTime : c1.tutorials) {
                            if (c2.lectures.contains(tutorialTime) || c2.labs.contains(tutorialTime) || c2.tutorials.contains(tutorialTime)) {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
    }

    public boolean semesterBalance(List<Course> cl) {
        if (courseCountOfSemester(cl, 1) > 4 || courseCountOfSemester(cl, 2) > 4) {
            return false;
        }
        return true;
    }

    public static List<List<Course>> combinations(List<Course> list, int k) {
        if (k == 0 || list.isEmpty()) {
            return Collections.emptyList();
        }
        if (k == 1) {
            return list.stream().map(e -> Stream.of(e).collect(toList())).collect(toList());
        }
        Map<Boolean, List<Course>> headAndTail = split(list, 1);
        List<Course> head = headAndTail.get(true);
        List<Course> tail = headAndTail.get(false);
        List<List<Course>> c1 = combinations(tail, (k - 1)).stream().map(e -> {
            List<Course> l = new ArrayList<>();
            l.addAll(head);
            l.addAll(e);
            return l;
        }).collect(Collectors.toList());
        List<List<Course>> c2 = combinations(tail, k);
        c1.addAll(c2);
        return c1;
    }

    public static Map<Boolean, List<Course>> split(List<Course> list, int n) {
        return IntStream
                .range(0, list.size())
                .mapToObj(i -> new AbstractMap.SimpleEntry<>(i, list.get(i)))
                .collect(partitioningBy(entry -> entry.getKey() < n, mapping(AbstractMap.SimpleEntry::getValue, toList())));
    }

    public int courseCountOfSemester(List<Course> courses, int semester) {
        int count = 0;
        for (Course course : courses) {
            if (course.semester == semester) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        CourseManager courseManager = new CourseManager();
        courseManager.start();
    }
}
